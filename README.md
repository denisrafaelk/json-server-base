**any doubts or suggestion contact-me Denis Rafael Korb on Slack or Discord**

- example of axios use

```
  const url = `https://json-server-order-here.herokuapp.com`;
  const api = axios.create({
    baseURL: url,
    headers: { "Content-Type": "application/json" },
  });
  api.get(`/products?category_like=pizza`).then((res) => {
    console.log(res);
  });
```
**ENDPOINTS**


- /categories

- [ ] _general categories_
```
    {
      "category": "category",
      "img": "image URL"
    }
```


- products
```
    {
      "product": "product",
      "img": "image URL",
      "description": "description",
      "price": 40,
      "category": "category"
    }
```
